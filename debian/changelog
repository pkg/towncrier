towncrier (24.8.0-2+apertis0) apertis; urgency=medium

  * Sync from debian/trixie.

 -- Apertis CI <devel@lists.apertis.org>  Mon, 10 Mar 2025 01:56:38 +0000

towncrier (24.8.0-2) unstable; urgency=medium

  * Remove unused “Build-Depends-Indep: python3-mock”.
    The upstream release uses only ‘unittest.mock’ from the standard library.
  * Switch build dependency to ‘python3-tomli’, specified in upstream project.
  * Resume installing Python library as a public shared library.
    Closes: bug#1087787. Thanks to Santiago Vila for the report; thanks to
    Colin Watson for the diagnosis.

 -- Ben Finney <bignose@debian.org>  Wed, 20 Nov 2024 17:07:04 +1100

towncrier (24.8.0-1) unstable; urgency=medium

  * The “Myrtle Cagle” release.
  * New upstream release.
    Highlights since previous release:
    * Support Python version 3.8 – 3.12.
    * Provide a default template if Markdown is selected.
    * Generated output no longer adds extra line breaks.
    * Gracefully handle news fragments that are not in VCS repository.
    * Prompt interactively when no fragment filename is specified.
  * Declare conformance to “Standards-Version: 4.7.0”.
    No additional changes needed.
  * Switch to ‘pyproject’ build system.
    Closes: bug#1083804. Thanks to Matthias Klose for the report.
  * debian/patches/:
    * Remove patches resolved in new upstream release.
  * debian/patches/test-current-version-in-output.patch:
    * Test the actual current version is in the test output.
  * Install to an application directory, not a shared Python library.

 -- Ben Finney <bignose@debian.org>  Sun, 20 Oct 2024 13:34:11 +1100

towncrier (22.12.0-1+apertis0) apertis; urgency=medium

  * Import from Debian bookworm.
  * Set component to development.

 -- Apertis package maintainers <packagers@lists.apertis.org>  Fri, 28 Apr 2023 09:12:31 +0530

towncrier (22.12.0-1) unstable; urgency=medium

  * The “Claudio Cappelli” release.

  [ Ben Finney ]

  * New upstream release.
    Highlights since previous release:
    * Support Python version 3.7 – 3.11.
    * The ‘check’ command now works for branches that change the news file.
    * The ‘create’ command now has a ‘-m TEXT’ option to define the content
      of the newly created fragment.
    * The ‘build’ command now has a ‘--keep’ option to keep fragments in
      place.
    * Support having the release notes for each version in a separate file.
    * Support fragments not associated with any issue.
    * Default branch for `towncrier check` is now "origin/main"
      (not "origin/master").
  * Declare conformance to “Standards-Version: 4.6.2”.
    No additional changes needed.
  * Declare Build-Depends for architecture-independent packages.
  * Remove a Lintian override for documentation files in wrong directory.
    The files should in fact not be in the package; Lintian is correct.
  * Remove cruft from the build directory left there by Twisted Trial.
    Closes: bug#1027992. Thanks to Chris Lamb for the report.
  * Update URLs for upstream metadata.
  * Merge ‘towncrier(1)’ manual page work from sergiosacj.
    Thanks to Sérgio de Almeida Cipriano Junior.

  [ Sérgio de Almeida Cipriano Junior ]

  * Update UScan configuration to query Python Package Index releases.

 -- Ben Finney <bignose@debian.org>  Thu, 12 Jan 2023 15:22:18 +1100

towncrier (21.9.0-2) unstable; urgency=medium

  * Invoke PyBuild with options to install as an application.
  * Update Lintian overrides to match current Lintian output.
  * Declare conformance to “Standards-Version: 4.6.1”.
    No additional changes needed.
  * Add Bash command completion for ‘towncrier’ command.
  * Declare ‘pybuild-plugin-pyproject’ build dependency to use Setuptools.
  * Declare build dependencies to run test suite.
  * Override Lintian false positive for package documentation.

 -- Ben Finney <bignose@debian.org>  Wed, 23 Nov 2022 12:18:20 +1100

towncrier (21.9.0-1) unstable; urgency=medium

  * The “Jerri Sloan” release.
  * New upstream release.
    Highlights since previous release:
    * Features now delegated to ‘towncrier’ sub-commands.
    * New ‘create’ sub-command, to create a news fragment.
    * New ‘check’ sub-command, to validate the news fragments.
    * New ‘--config’ option to specify configuration file.
    * Allow configuration of project ‘version’ and ‘name’.
  * Document the copyright status of ‘CODE_OF_CONDUCT.md’.
  * Use the verbatim copyright attribution for the project files.
  * Declare conformance to “Standards-Version: 4.6.0”.
  * Remove obsolete field from DEP-12 metadata.
  * Document the current main packaging development VCS branch.
  * Update the canonical Debian packaging VCS repository.
  * Update the project homepage.
    Closes: bug#986319. Thanks to Paul Wise for the report.
  * Update the URL for fetching upstream source.
  * Add specification for AutoPkgTest of the installed package.
  * Override false positive Lintian check for VCS-* field names.
  * Add a manual page document for the ‘towncrier’ command.
  * Install the upstream README document.
  * Install the upstream change log document.

 -- Ben Finney <bignose@debian.org>  Wed, 27 Apr 2022 22:14:36 +1000

towncrier (19.2.0-1) unstable; urgency=medium

  * The “Tarishi Jain” release.
  * Initial Debian release.
    Closes: bug#927454.

 -- Ben Finney <bignose@debian.org>  Mon, 08 Mar 2021 12:26:00 +1100



